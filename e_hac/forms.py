from django import forms
from .models import EHac

class EHacForm(forms.ModelForm):
	class Meta:
		model = EHac
		fields = '__all__'
	error_messages = {
		'required' : 'Please Type'
	}