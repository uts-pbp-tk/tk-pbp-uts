from django.forms.widgets import EmailInput
from django.shortcuts import render,redirect
from .forms import dataForm
from .models import data_PCR
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/user/login/')
def daftar_awal(request):
    form = dataForm()
    if request.method == 'POST':
        form = dataForm(request.POST)
        if form.is_valid():
            email = request.user
            lokasi = request.POST.get('lokasi')
            waktu = request.POST.get('waktu')
            data_PCR.objects.create(
                email=email,
                lokasi=lokasi,
                waktu=waktu
            )
            # form.save()
            return redirect('/user/afterForm/')

    context = {'form':form}
    return render(request, 'daftar_awal.html', context)

def daftar_akhir(request):
    dataDiri = data_PCR.objects.all().values()
    response = {'dataDiri' : dataDiri}
    return render(request, 'daftar_akhir.html', response)
