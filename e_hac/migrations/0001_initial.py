# Generated by Django 3.2.7 on 2021-11-05 16:48

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EHac',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=30)),
                ('kewarganegaraan', models.CharField(max_length=20)),
                ('jenis_kelamin', models.CharField(max_length=10)),
                ('dob', models.DateField()),
                ('nik', models.CharField(max_length=16)),
                ('kota_asal', models.CharField(max_length=70)),
                ('kota_tujuan', models.CharField(max_length=70)),
                ('alamat', models.CharField(max_length=50)),
                ('tanggal_berangkat', models.DateField()),
                ('tanggal_sampai', models.DateField()),
                ('jenis_kendaraan', models.CharField(max_length=20)),
                ('keterangan', models.CharField(max_length=15)),
                ('pernyataan_kesehatan', models.CharField(max_length=50)),
            ],
        ),
    ]
