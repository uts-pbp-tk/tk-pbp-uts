from rest_framework.serializers import ModelSerializer
from .models import EHac

class EhacSerializer(ModelSerializer):
    class Meta:
        model = EHac
        fields = '__all__'