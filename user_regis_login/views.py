from django.http import response
from django.http.response import HttpResponseRedirect
from django.contrib.auth.models import User as DjangoUser
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from .forms import RegisForm
from aplikasi_vaksin.models import User
from json import dumps

# Create your views here.
def user_register(request):
  # create object of form
  regis_form = RegisForm(request.POST or None, request.FILES or None)
  users = User.objects.all().values()
  users = User.objects.all().values()
  user_as_json = []
  for user in users:
    user_as_json.append({'email':user['email'], 'password': user['password']})
    
  # check if form data is valid
  if regis_form.is_valid():
    # save the form data to model
    regis_form.save()
  response = {
    "forms": regis_form,
    'users' : users,
    'json': dumps(user_as_json),
    "icons" : [
      "user_regis/icon/email.png",
      "user_regis/icon/key.png",
      "user_regis/icon/person.png",
      "user_regis/icon/phone.png",
      "user_regis/icon/age.png",
      "user_regis/icon/id.png",
      "user_regis/icon/location.png",
      ]
  }
  if request.method == "POST":
    email = request.POST['email']
    password = request.POST['password']
    name = request.POST['name']
    user = DjangoUser.objects.create_user(email, email, password)
    user.first_name = name
    user.save()
    # redirect
    return redirect('user_login')
    # return HttpResponseRedirect("/lab-4")
  return render(request, "register/index.html", response)


def user_login(request):
  users = User.objects.all().values()
  user_as_json = []
  for user in users:
    user_as_json.append({'email':user['email'], 'password': user['password']})
  response = {
    'users' : users,
    'json': dumps(user_as_json),
  }
  if request.method == "POST":
    # saat berhasil login, return ke home
    email = request.POST['email']
    password = request.POST['password']
    user = authenticate(request, username=email, password=password)
    if user is not None:
      login(request, user)
      return HttpResponseRedirect('/')
  return render(request, "user_login/index.html", response)

def user_logout(request):
  logout(request)
  return HttpResponseRedirect("/user/login")