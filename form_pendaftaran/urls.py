from django.urls import path

from . import views

urlpatterns = [
    path('add/', views.daftar_vaksin, name='register_vaksin'),
    path('update/', views.user_update_view, name='update_user'),
    path('ajax/load-tanggal/', views.load_tanggal, name='ajax_load_tanggal'), # AJAX
    path('ajax/load-jenis/', views.load_jenis, name='ajax_load_jenis')
]