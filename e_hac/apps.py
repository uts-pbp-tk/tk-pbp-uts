from django.apps import AppConfig


class EHacConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'e_hac'
