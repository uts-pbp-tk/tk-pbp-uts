const emails = document.getElementsByClassName('emails');
const passwords = document.getElementsByClassName('passwords');
function email_is_valid(){
  // for (let i = 0; i<emails.length; i++){
  //   // console.log(emails[i].innerHTML);
  //   if ($('#id_email').val() == emails[i].innerHTML) return false;
  // }
  // return true;
  for (let x in json) {
    if ($('#id_email').val() == json[x]['email']) return false;
    // console.log(users_as_json[user]['email']);
    // console.log('email: ' + user['email'] + ", password: "+ user['password'])
  }
  return true;
}
function password_is_valid(){
  for (let x in json) {
    if ($('#id_password').val() == json[x]['password']) return false;
    // console.log(users_as_json[user]['email']);
    // console.log('email: ' + user['email'] + ", password: "+ user['password'])
  }
  return true;
}

$(document).ready(function () {
  $('#id_usia').on('change', function (e) {
    if (e.target.value < 0){
      $('#id_usia').val(0);
    } else if (e.target.value > 150){
      $('#id_usia').val(150);
    }
  });
  $("#id_email").on('input', function (e) {
    if ($('#id_email').val() == ''){
      $('#email_message').text("Email tidak boleh kosong");
      $('#email_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    } else if (!$('#id_email').val().includes("@")){
      $('#email_message').text("Email tidak valid");
      $('#email_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    }
    else if (email_is_valid()){
      $('#email_message').text("Email anda valid");
      $('#email_message').css({
        'z-index':'2',
        'color' : 'blue',
      });
      return true;
    } else {
      $('#email_message').text("Maaf, email sudah terdaftar");
      $('#email_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    }
  });
  $("#id_password").on('input', function (e) {
    if ($('#id_password').val() == '') {
      // e.preventDefault();
      $('#password_message').text("Password tidak boleh kosong");
      $('#password_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    } else if ($('#id_password').val().length < 8){
      // e.preventDefault();
      $('#password_message').text("Password tidak cukup kuat");
      $('#password_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    } else if (!password_is_valid()){
      // e.preventDefault();
      $('#password_message').text("Maaf, password sudah terpakai");
      $('#password_message').css({
        'z-index':'2',
        'color' : 'red',
      });
      // console.log(e.target.value);
    }  else {
      $('#password_message').css({
        'z-index':'-3',
        'color' : 'red',
      });
      return true;
    }
  });
  $("button").on('click', function (e) {
    if ( $('#id_email').val() == ''         || 
        !$('#id_email').val().includes("@") ||
        !email_is_valid()                   ||
         $('#id_password').val() == ''      ||
         $('#id_password').val().length < 8 ||
         !password_is_valid())  {
      e.preventDefault();
    } //else if (!password_is_valid()){
    //   e.preventDefault();
    //   $('#password_message').text("Maaf, password tidak sesuai");
    //   $('#password_message').css({
    //     'z-index':'2',
    //     'color' : 'red',
    //   });
    //   // console.log(e.target.value);
    // }  
  });
  $('#id_password').focus(function (e) { 
    e.preventDefault();
    $('#password_message').css({
      'z-index':'-3',
      'color' : 'red',
    });
  });
});
