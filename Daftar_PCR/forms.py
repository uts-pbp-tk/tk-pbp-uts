from django.forms import ModelForm, widgets
from .models import data_PCR
from django import forms


class dataForm(ModelForm):
    class Meta:
        model = data_PCR
        fields = ("lokasi", "waktu")

        widgets = {
            'waktu': forms.TextInput(
                attrs={
                    'type': 'date',
                }
            )
        }
