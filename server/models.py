from django.db import models

# Create your models here.
class Pengguna(models.Model):
    email1 = models.EmailField()
    password1 = models.CharField(max_length=30)
    name1 = models.CharField(max_length=30)
    no_hp1 = models.CharField(max_length=12)
    usia1 = models.CharField(max_length=3, blank=True, default="-")
    nik1 = models.CharField(max_length=15, blank=True, default="-")
    domisili1 = models.CharField(max_length=50, blank=True, default="-")
    lokasi1 = models.CharField(max_length=50, blank=True, default="-")
    tanggal1 = models.CharField(max_length=50, blank=True, default="-")
    jenis1 = models.CharField(max_length=50, blank=True, default="-")
    def __str__(self) -> str:
        return f"{self.name1} | {self.email1}"