from django.urls import path
from .views import cek_faskes

urlpatterns = [
      path('faskes/', cek_faskes, name="cek_faskes")
]