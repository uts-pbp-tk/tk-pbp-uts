import urllib
from django import forms
from django.forms import widgets
from django.forms.models import fields_for_model
from django.forms.widgets import TextInput, Textarea
from aplikasi_vaksin.models import User
from django.http import QueryDict
from django.forms.models import model_to_dict
from admin_tambah_faskes.models import Lokasi, TanggalWaktu, JenisVaksin

class DaftarVaksin(forms.ModelForm):
  """Form definition for Note."""

  class Meta:
    """Meta definition for Noteform."""
    model = User
    fields = ('email', 'password', 'name', 'no_hp', 'usia', 'nik', 'domisili','lokasi','tanggal', 'jenis')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tanggal'].queryset = TanggalWaktu.objects.none()

        if 'lokasi' in self.data:
            try:
                lokasi_id = int(self.data.get('lokasi'))
                self.fields['tanggal'].queryset = TanggalWaktu.objects.filter(lokasi_id=lokasi_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['tanggal'].queryset = self.instance.lokasi.tanggal_set.order_by('name')
    
    
        self.fields['jenis'].queryset = JenisVaksin.objects.none()
        if 'tanggal' in self.data:
            try:
                tanggal_id = int(self.data.get('tanggal'))
                self.fields['jenis'].queryset = JenisVaksin.objects.filter(tanggal_id=tanggal_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['jenis'].queryset = self.instance.tanggal.jenis_set.order_by('name')
   
    # custom label
    labels = {
      'email' : 'Email',
      'password' : 'Password',
      'name' : 'Nama Lengkap',
      'no_hp' : 'No Handphone',
      'usia' : 'Usia',
      'nik' : 'NIK',
      'domisili' : 'Domisili',
      'lokasi' : 'Lokasi',
    }
    """
    ** custom witdgets **
    attrs itu digunakan untuk mengatur atribut dari input yang kita gunakan. misal kita ingin tag input kita seperti ini 
    <input id='id_input' placeholder='input' type='text' >
    maka kita tinggal isi attrs dengan dictionary yang keynya adalah nam_atribut dan valuenya adalah value yang kita inginkan. Usahakan kita tidak salah memasukkan atribut dan valuenya biar tidak jadi error :D
    """
    widgets = {
      'email' : forms.TextInput(
        attrs={
          'type' : 'email',
          'placeholder' : 'Email',
          'class':'form-control',
          'aria-describedby':'inputGroupPrepend',
        }
      ),
      'password' : forms.TextInput(
        attrs={
          'type' : 'password',
          'placeholder' : 'Password',
          'class':'form-control',
          'aria-describedby':'inputGroupPrepend',
        }
      ),
      'name' : forms.TextInput(
        attrs={
          'type' : 'text',
          'placeholder' : 'Nama lengkap',
          'class':'form-control',
          'aria-describedby':'inputGroupPrepend',
        }
      ),
      'no_hp' : forms.TextInput(
        attrs={
          'type' : 'tel',
          'placeholder' : ' No handphone',
          'class':'form-control',
          'aria-describedby':'inputGroupPrepend',
        }
      ),
      'usia' : forms.TextInput(
        attrs={
          'type' : 'number',
          'min'  :'0',
          'max' :'150',
          'placeholder' : 'Usia',
          'class':'form-control',
          'aria-describedby':'inputGroupPrepend',
          'required': 'required',
        }
      ),
      'nik' : forms.TextInput(
        attrs={
          'type' : 'number',
          'placeholder' : 'NIK',
          'class':'form-control',
          'aria-describedby':'inputGroupPrepend',
          'required': 'required',
        }
      ),
      'domisili' : forms.TextInput(
        attrs={
          'type' : 'text',
          'placeholder' : 'Domisili',
          'class':'form-control',
          'aria-describedby':'inputGroupPrepend',
          'required': 'required',
        }
      ),
      'lokasi' : forms.Select()
    }
    