from django.shortcuts import render
from django.contrib.auth.models import User as DjangoUser
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import EditProfileForm, ProfileForm 
from aplikasi_vaksin.models import User
from json import dumps

@login_required
def edit_profile(request):
    from django.shortcuts import render
    from aplikasi_vaksin.models import User

    # Create your views here.
    def index(request):
        users = User.objects.all().values()
        print('user now', request.user)
        name_user = ''
        for user in users:
            if user['email'] == str(request.user):
                name_user = user['name']
                break
        if name_user != '':
            if len(name_user) > 10:
                if len(name_user.split()[0]) > 10:
                    name_user = name_user.split()[0][:11] + "..."
                else:
                    name_user = name_user.split()[0]
        return render(request, 'home/index.html', {'name': name_user})

@login_required
def profile(request): 
    p = User.objects.filter(email=request.user.email,name=request.user.first_name)
    context = {
        'pengguna' : p[0]
    }
    return render(request, 'view_profile.html', context)



    # 'email' : User.objects.filter(User.email),
    # 'name' : User.objects.filter(User.name),
    # 'no_hp' : User.objects.filter(User.no_hp),
    # 'usia' : User.objects.filter(User.usia),
    # 'nik' : User.objects.filter(User.nik),
    # 'domisili' : User.objects.filter(User.domisili),