from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import EHac
from .forms import EHacForm
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import EhacSerializer

# Create your views here.

def add_ehac(request):
    form = EHacForm()

    if request.method == 'POST':
        form = EHacForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('ehac-list')

    response = {'form' : form} 
    return render(request, 'ehac_form.html',response)

def ehac_list(request):
    ehac = EHac.objects.all().values()
    response = {'ehac' : ehac}

    return render(request, 'ehac_list.html', response)

#return all ehac/ehac list
@api_view(['GET'])
def getEhacs(request):
    ehacs = EHac.objects.all()
    serializer = EhacSerializer(ehacs, many=True)
    return Response(serializer.data)

#return single ehac/ detailed one based on its id
@api_view(['GET'])
def getEhac(request, pk):
    ehac = EHac.objects.get(id=pk)
    serializer = EhacSerializer(ehac, many=False)
    return Response(serializer.data)

#sending data to ehac server
@api_view(['POST'])
def createEhac(request):
    
    serializer = EhacSerializer(data = request.data)
    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)

#update ehac data in server
@api_view(['POST'])
def updateEhac(request, pk):
    ehac = EHac.objects.get(id=pk)

    serializer = EhacSerializer(instance=ehac, data = request.data)

    if serializer.is_valid():
        serializer.save()
        
    return Response(serializer.data)

@api_view(['DELETE'])
def deleteEhac(request, pk):
    ehac = EHac.objects.get(id=pk)
    ehac.delete()
        
    return Response("Ehac berhasil dihapus")