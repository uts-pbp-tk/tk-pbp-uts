from django.urls import path
from . import views

urlpatterns = [
    path('user_profile/', views.profile, name='user_profile'),
    path('user_profile/edit_profile', views.edit_profile, name='edit_profile')
]