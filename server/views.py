from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from aplikasi_vaksin.models import User
from .models import Pengguna
from e_hac.models import EHac
from django.contrib.auth.models import User as DjangoUser
from e_hac.models import EHac
from e_hac.forms import EHacForm

# Create your views here.
@csrf_exempt
def vaksin_server(request):
    print(request.method)
    print(request.body)
    user = Pengguna.objects.all().values()
    data = {}
    if request.method == 'POST':
        email1 = request.POST['email']
        password1 = request.POST['password']
        name1 = request.POST['name']
        no_hp1 = request.POST['no_hp']
        usia1 = request.POST['usia']
        nik1 = request.POST['nik']
        domisili1 = request.POST['domisili']
        lokasi1 = request.POST['lokasi']
        tanggal1 = request.POST['tanggal']
        jenis1 = request.POST['jenis']
        
        Pengguna.objects.create(
            email1= email1,
            password1= password1,
            name1= name1,
            no_hp1= no_hp1,
            usia1=usia1,
            nik1=nik1,
            domisili1=domisili1,
            lokasi1=lokasi1,
            tanggal1=tanggal1,
            jenis1=jenis1
        )
        data = {
            "email": email1,
            "password": password1,
            "name": name1,
            "no_hp": no_hp1,
            "usia": usia1,
            "nik": nik1,
            "domisili": domisili1,
            "lokasi": lokasi1,
            "tanggal": tanggal1,
            "jenis": jenis1
        }
        # data1 = request.POST['data1'] # 'data1' disesuaikan dengan request yang diterima
        # data2 = request.POST['data2'] # 'data2' disesuaikan dengan request yang diterima
        # data = {
        #   'data1': data1,
        #   'data2': data2
        # }

    return JsonResponse(data, safe=False)
@csrf_exempt
def ehac_server(request):
    ehac = EHac.objects.all().values()
    data = {}
    print("user================")
    usera = User.objects.get(email="aman4@gmail.com")
    print(User.objects.all().values())
    if request.method == 'POST':
        nama = request.POST['nama']
        kewarganegaraan = request.POST['kewarganegaraan']
        jenis_kelamin = request.POST['jenis_kelamin']
        dob = request.POST['dob']
        nik = request.POST['nik']
        kota_asal = request.POST['kota_asal']
        kota_tujuan = request.POST['kota_tujuan']
        alamat = request.POST['alamat']
        tanggal_berangkat = request.POST['tanggal_berangkat']
        tanggal_sampai = request.POST['tanggal_sampai']
        jenis_kendaraan = request.POST['jenis_kendaraan']
        keterangan = request.POST['keterangan']
        pernyataan_kesehatan = request.POST['pernyataan_kesehatan']

        EHac.objects.create(
            nama=nama,
            kewarganegaraan=kewarganegaraan,
            jenis_kelamin=jenis_kelamin,
            dob=dob,
            nik=nik,
            kota_asal=kota_asal,
            kota_tujuan=kota_tujuan,
            alamat=alamat,
            tanggal_berangkat=tanggal_berangkat,
            tanggal_sampai=tanggal_sampai,
            jenis_kendaraan=jenis_kelamin,
            keterangan=keterangan,
            pernyataan_kesehatan=pernyataan_kesehatan
        )
        data = {
            "nama": nama,
            "kewarganegaraan": kewarganegaraan,
            "jenis_kelamin": jenis_kelamin,
            "dob": dob,
            "nik": nik,
            "kota_asal": kota_asal,
            "kota_tujuan": kota_tujuan,
            "alamat": alamat,
            "tanggal_berangkat": tanggal_berangkat,
            "tanggal_sampai": tanggal_sampai,
            "jenis_kendaraan": jenis_kendaraan,
            "keterangan": keterangan,
            "pernyataan_kesehatan": pernyataan_kesehatan,
        }
    return JsonResponse(data, safe=False)

@csrf_exempt
def login_server(request):
    user = User.objects.all().values()
    data = {}
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        msg = ""
        if not user.filter(email=email):
            msg = "Email belum terdaftar"
            namaUser = ""
        else:
            passUser = user.get(email=email).get("password")
            namaUser = user.get(email=email).get("name")
            if password != passUser:
                msg = "password tidak sesuai"
            else:
                msg = "OK"

        data = {
            "msg": msg,
            "email": email,
            "name": namaUser,
            "password": password
        }
        # data1 = request.POST['data1'] # 'data1' disesuaikan dengan request yang diterima
        # data2 = request.POST['data2'] # 'data2' disesuaikan dengan request yang diterima
        # data = {
        #   'data1': data1,
        #   'data2': data2
        # }

    return JsonResponse(data, safe=False)
@csrf_exempt
def regis_server(request):
    user = User.objects.all().values()
    data = {}
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        nama = request.POST['nama']
        no_hp = request.POST['no_hp']
        msg = ""
        if user.filter(email=email):
            msg = "Email sudah terdaftar"
        else:
            User.objects.create(
                email=email, 
                password=password,
                name=nama,
                no_hp=no_hp
            )
            user = DjangoUser.objects.create_user(email, email, password)
            user.first_name = nama
            user.save()
            msg = "OK"

        data = {
            "msg": msg,
        }
        # data1 = request.POST['data1'] # 'data1' disesuaikan dengan request yang diterima
        # data2 = request.POST['data2'] # 'data2' disesuaikan dengan request yang diterima
        # data = {
        #   'data1': data1,
        #   'data2': data2
        # }

    return JsonResponse(data, safe=False)
@csrf_exempt
def user_server(request):
    user = User.objects.all().values()
    data = {}
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        msg = "OKE"
        # if not user.filter(email=email):
        #     msg = "Email belum terdaftar"
        #     namaUser = ""
        # else:
        #     passUser = user.get(email=email).get("password")
        #     namaUser = user.get(email=email).get("name")
        #     if password != passUser:
        #         msg = "password tidak sesuai"
        #     else:
        #         msg = "OK"
        no_hp = user.get(email=email).get("no_hp")
        usia = user.get(email=email).get("usia")
        nik = user.get(email=email).get("nik")
        domisili = user.get(email=email).get("domisili")
        data = {
            "msg": msg,
            "no_hp": str(no_hp),
            "usia": str(usia),
            "nik": str(nik),
            "domisili": domisili,
        }
        # data1 = request.POST['data1'] # 'data1' disesuaikan dengan request yang diterima
        # data2 = request.POST['data2'] # 'data2' disesuaikan dengan request yang diterima
        # data = {
        #   'data1': data1,
        #   'data2': data2
        # }

    return JsonResponse(data, safe=False)
