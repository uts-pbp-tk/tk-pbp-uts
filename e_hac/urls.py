from django.urls import path
from django.urls.resolvers import URLPattern
from .views import add_ehac, ehac_list, getEhacs, getEhac, createEhac, updateEhac, deleteEhac

urlpatterns = [
    path('add-ehac', add_ehac, name='add_ehac'),
    path('ehac-list', ehac_list, name='ehac_list'),
    path('ehac_server/', getEhacs, name='getEhacs'),
    path('ehac_server/create/', createEhac, name='createEhac'),
    path('ehac_server/<str:pk>/', getEhac, name='getEhac'),
    path('ehac_server/update/<str:pk>/', updateEhac, name='updateEhac'),
    path('ehac_server/delete/<str:pk>/', deleteEhac, name='deleteEhac'),
]