from django.contrib import admin

# Register your models here.
from .models import  Lokasi, TanggalWaktu, JenisVaksin
admin.site.register(Lokasi)
admin.site.register(TanggalWaktu)
admin.site.register(JenisVaksin)