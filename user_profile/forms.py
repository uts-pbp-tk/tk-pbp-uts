import urllib
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import widgets
from django.forms.models import fields_for_model
from django.forms.widgets import TextInput, Textarea
from aplikasi_vaksin.models import User as UserProfile

class ProfileForm():
    class Meta:
        model = UserProfile
        fields = ('email', 'password', 'name', 'no_hp', 'usia', 'nik', 'domisili')


class EditProfileForm():
    class Meta:
        model = User
        fields = ('email', 'password', 'name', 'no_hp', 'usia', 'nik', 'domisili')

    # custom label
    labels = {
      'email' : 'Email',
      'password' : 'Password',
      'name' : 'Nama Lengkap',
      'no_hp' : 'No Handphone',
      'usia' : 'Usia',
      'nik' : 'NIK',
      'domisili' : 'Domisili',
    }
    """
    ** custom witdgets **
    attrs itu digunakan untuk mengatur atribut dari input yang kita gunakan. misal kita ingin tag input kita seperti ini 
    <input id='id_input' placeholder='input' type='text' >
    maka kita tinggal isi attrs dengan dictionary yang keynya adalah nam_atribut dan valuenya adalah value yang kita inginkan. Usahakan kita tidak salah memasukkan atribut dan valuenya biar tidak jadi error :D
    """
    widgets = {
      'email' : forms.TextInput(
        attrs={
          'type' : 'email',
          'placeholder' : 'Email'
        }
      ),
      'password' : forms.TextInput(
        attrs={
          'type' : 'password',
          'placeholder' : 'Password',
        }
      ),
      'name' : forms.TextInput(
        attrs={
          'type' : 'text',
          'placeholder' : 'Nama lengkap',
        }
      ),
      'no_hp' : forms.TextInput(
        attrs={
          'type' : 'number',
          'placeholder' : ' No handphone',
        }
      ),
      'usia' : forms.TextInput(
        attrs={
          'type' : 'number',
          'min'  :'0',
          'max' :'150',
          'placeholder' : 'Usia',
        }
      ),
      'nik' : forms.TextInput(
        attrs={
          'type' : 'number',
          'placeholder' : 'NIK',
        }
      ),
      'domisili' : forms.TextInput(
        attrs={
          'type' : 'text',
          'placeholder' : 'Domisili',
        }
      )
    }
