from django.apps import AppConfig


class UserregisloginConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    # name = 'userRegisLogin'
    name = 'user_regis_login'
