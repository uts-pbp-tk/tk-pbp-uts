"""uts_e02 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('aplikasi_vaksin.urls')),
    path('', include('server.urls')),
    path('user/', include('user_regis_login.urls')),
    path('user/', include('admin_tambah_faskes.urls')),
    path('user/', include('form_pendaftaran.urls')),
    path('user/', include('Daftar_PCR.urls')),
    path('user/', include('e_hac.urls')),
    path('user/', include('user_profile.urls')),
]
"""
mungkin kalau yang berhubungan dengan user, awalan path bisa pakai 'user/' aja, biar sama gitu. misal path('user/' include('profile.urls')). yang penting di urls tiap app dibedakan gitu. misal path('dashboard/', dashboard, name='dashboard') untuk dashboard, path('self_assesment/', self_assesment, name='self_assesment') untuk self_assesment, dst dalam app yang sama misal profile
"""