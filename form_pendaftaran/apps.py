from django.apps import AppConfig


class FormPendaftaranConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'form_pendaftaran'
