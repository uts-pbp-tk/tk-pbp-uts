# Generated by Django 3.2.7 on 2021-12-31 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pengguna',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email1', models.EmailField(max_length=254)),
                ('password1', models.CharField(max_length=30)),
                ('name1', models.CharField(max_length=30)),
                ('no_hp1', models.CharField(max_length=12)),
                ('usia1', models.CharField(blank=True, default='-', max_length=3)),
                ('nik1', models.CharField(blank=True, default='-', max_length=15)),
                ('domisili1', models.CharField(blank=True, default='-', max_length=50)),
                ('lokasi1', models.CharField(blank=True, default='-', max_length=50)),
                ('tanggal1', models.CharField(blank=True, default='-', max_length=50)),
                ('jenis1', models.CharField(blank=True, default='-', max_length=50)),
            ],
        ),
    ]
