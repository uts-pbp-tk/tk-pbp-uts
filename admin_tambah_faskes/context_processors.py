#render template with request
from .models import Lokasi, TanggalWaktu, JenisVaksin
def get_notes(request):
    return {'collections': Lokasi.objects.all()}
def get_tanggal(request):
    return {'tanggal': TanggalWaktu.objects.all()}
def get_jenis(request):
    return {'jenis': JenisVaksin.objects.all()}