from django.urls import path
from .views import  login_server, user_server, regis_server, vaksin_server, ehac_server


urlpatterns = [
  # path('', index, name='index'),
  path('login_server/', login_server, name='login_server'),
  path('user_server/', user_server, name='user_server'),
  path('regis_server/', regis_server, name='regis_server'),
  path('ehac_server/', ehac_server, name='ehac_server'),
  path('vaksin_server', vaksin_server, name='vaksin_server'),
  # path('form/', add_note, name='add_list'),
]