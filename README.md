# TK-PBP-UTS
Deploying for heroku with tutorial from https://medium.com/@ganiilhamirsyadi/how-to-deploy-django-app-on-heroku-using-gitlab-ci-cd-step-by-step-45c0d9d31798

**Anggota Kelompok E02**:<br>
Savira Rosa Meidiana - 1906399682<br>
M. Saleh Januri - 2006464625<br>
Madeleine Sekar Putri Wijayanto - 2006483164<br>
Abdul Rahman - 2006463572<br>
Nirwana Pratiwi - 1906399051<br>
Irsyad Nurachim Fadjar - 2006464820<br>

**Herokuapp**:https://proyek-uts-e02.heroku.app.com/

**Gitlab**:
https://gitlab.com/uts-pbp-tk

**Fitur & Pembagian Persona**:<br>
Home page & Navbar & Profile User : Nirwana Pratiwi <br>
- Fitur Home Page & Navigation Bar akan menyediakan halaman awal aplikasi dan shortcut pada setiap halaman yang dipilih oleh user (termasuk informasi log-in dan sign-up)

User side - Form pendaftaran vaksin : Madeleine Sekar <br>
- Halaman form pendaftaran vaksin akan menampilkan tempat penyelenggara vaksin yang masih memiliki kuota pendaftar. 
- Setelah user memilih lokasi, akan ditampilkan jadwal yang tersedia. 
- Kemudian pendaftar akan diarahkan ke dalam form pendaftaran dimana pendaftar mengisi data diri.
- Setelah itu, user akan di redirect ke dalam form yang berisi self-assessment untuk mengisi kelayakan pendaftaran. 
- Jika pendaftar sudah mendaftar, akan di redirect ke login.

Admin side - tambah lokasi : Abdul Rahman<br>
- Fitur ini akan tersedia dalam admin side yang berguna untuk mendaftarkan untuk penyelenggara vaksin.

Admin side - tambah jadwal yg tersedia (pemberitahuan jadwal fix) Savira Rosa<br>
- Fitur admin ini berguna untuk menambahkan jadwal.

Admin side - tambah kuota & database pendaftar: M. Saleh Januri<br>
- Fitur admin ini berguna untuk menambahkan kuota & menampilkan database pendaftar (tabel HTML).

User side - (selain form: page apakah dia memenuhi syarat atau gk buat daftar) & after-booking page (thankyou & data diri & jadwal >>> tabel HTML) : Irsyad Nurachim <br>
- Berisikan form yang mengecek apakah ia memenuhi syarat untuk mendaftar
- Redirect ke after-booking page
	
**Uraian Singkat Aplikasi**:<br>
Aplikasi yang akan kami ajukan adalah aplikasi pendaftaran vaksin terpusat. Aplikasi ini akan mempermudah pengguna untuk menemukan lokasi pendaftaran vaksin terdekat. Selain itu, aplikasi ini juga akan mempermudah penyelenggara vaksin untuk menyeleksi dan mendata peserta vaksin yang eligible. 



