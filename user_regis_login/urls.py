from django.urls import path
from .views import  user_login, user_logout, user_register

urlpatterns = [
  # path('', index, name='index'),
  path('register/', user_register, name='user_register'),
  path('login/', user_login, name='user_login'),
  path('logout/', user_logout, name='user_logout'),
  # path('form/', add_note, name='add_list'),
]