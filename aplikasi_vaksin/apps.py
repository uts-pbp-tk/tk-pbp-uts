from django.apps import AppConfig


class AplikasiVaksinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aplikasi_vaksin'
