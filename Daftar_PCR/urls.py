from django.urls import path
from .views import daftar_awal, daftar_akhir

urlpatterns = [
  path('beforeForm/', daftar_awal, name='daftar_awal'),
  path('afterForm/', daftar_akhir, name='daftar_akhir'),
]