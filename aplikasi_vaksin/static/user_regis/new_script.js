$(document).ready(function() {
  (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation');
    const emails = document.getElementsByClassName('emails');
    const inputs = document.querySelectorAll('.form-control');
    const email_input = document.getElementById('id_email');
    const no_hp_input = document.getElementById('id_no_hp');
    const email_feedback = email_input.nextElementSibling;
    const no_hp_feedback = no_hp_input.nextElementSibling;
    // email_input.addEventListener('blur', email_validation);
    const email_afterValidated = document.querySelector(".form-control#id_email");
    email_input.addEventListener('input', email_validation);
    for (let ii = 1; ii<inputs.length; ii++){
    // const action = default_validation(inputs[ii]);
    inputs[ii].addEventListener('input', function(el){
      default_validation(el.target);
    });
    }
    // no_hp_input.addEventListener('input', no_hp_validation);

    const email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    const no_hp_pattern = /^\d+$/
    function default_validation(input) {
      const input_feedback = input.parentElement.lastChild;
      if (input.value == ""){
        input_feedback.innerHTML = "Entri ini tidak boleh kosong";
        input.classList.add('is-invalid');
        return false;
      }
      if (input.getAttribute('type') != 'text' &&
        input.getAttribute('type') != 'password'){
        if (no_hp_pattern.test(input.value)){
          input.classList.remove('is-invalid');
          input.classList.add('is-valid');
          return true
        } else {
          input_feedback.innerHTML = "No Handphone tidak valid";
          input.classList.add('is-invalid');
          return false;
        }
      } 
      input.classList.remove('is-invalid');
      input.classList.add('is-valid');
      return true;
    }
    function email_is_valid(){
      for (let x in json) {
        if ($('#id_email').val() == json[x]['email']) return false;
      }
      return true;
    }
    function email_validation() {
      if (!email_is_valid()){
        console.log("email sudah terdaftar");
        email_feedback.innerHTML = "Email sudah terdaftar";
        email_input.classList.add('is-invalid');
        // email_input.setAttribute('aria-invalid', true);
        return false;
      } 
      if (email_input.value == "") {
        console.log("email kosong");
        email_feedback.innerHTML = "Entri ini tidak boleh kosong";
        email_input.classList.add('is-invalid');
        // email_input.setAttribute('aria-invalid', true);
        return false;
      } 
      if (email_pattern.test(email_input.value)) {
        console.log("email valid");
        email_input.classList.remove('is-invalid');
        email_input.classList.add('is-valid');
        // email_input.setAttribute('aria-invalid', false);
        // email_input.classList.add();
        return true;
      } else {
        console.log("email tidak valid");
        email_feedback.innerHTML = "Email tidak valid";
        email_input.classList.add('is-invalid');
        return false;
      }
    }


  // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        console.log(event.target);
        if (!email_validation() || !form.checkValidity() ) {
          event.preventDefault();
          event.stopPropagation();
        } else {
        }
        form.classList.add('was-validated');

      }, false)
    })
  })();
  $("#password-eye").on('click', function(event) {
    console.log("oke");
      event.preventDefault();
      if($('#id_password').attr("type") == "text"){
          $('#id_password').attr('type', 'password');
          $('#password-eye i').addClass( "bi-eye-slash-fill" );
          $('#password-eye i').removeClass( "bi-eye-fill" );
      }else if($('#id_password').attr("type") == "password"){
          $('#id_password').attr('type', 'text');
          $('#password-eye i').removeClass( "bi-eye-slash-fill" );
          $('#password-eye i').addClass( "bi-eye-fill" );
      }
  });
});